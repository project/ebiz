About Ebiz Grazitti Theme
====================
Ebiz Grazitti is a Drupal 8 theme. The theme is not dependent on any
core theme. Its very light weight for fast loading with modern look.
  Simple and clean design
  Drupal standards compliant
  Implementation of a JS Slideshow
  Multi-level drop-down menus
  Footer with 4 regions
  A total of 10 regions
  Compatible and tested on IE7, IE8, IE9+, Opera, Firefox, Chrome browsers

Browser compatibility:
=====================
The theme has been tested on following browsers. IE7+, Firefox, Google Chrome, Opera.

Drupal compatibility:
=====================
This theme is compatible with Drupal 8.x.x

Designed & Developed for Drupal 8 by:
=====================================
https://www.drupal.org/grazitti-interactive
https://www.grazitti.com

Ported to Drupal 8 by:
======================
https://www.drupal.org/u/dipakmdhrm
http://www.dipakyadav.com

Maintainers:
======================
https://www.drupal.org/u/himanshu_batra_grazitti
https://www.drupal.org/u/rohit_sharma_grazitti

Need Help
=====================
https://www.grazitti.com/contactus/
